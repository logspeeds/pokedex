import styled, { css } from "styled-components/native";
import theme from "../../global/styles/theme";

export const Container = styled.TouchableOpacity`
${({ theme }) => css`
width: 100%;
height: 50px;

border-radius: 10px;

justify-content: center;
align-items: center;

background-color: ${theme.colors.type.water};
`}
`;

export const Title = styled.Text`
font-size: 14px;
color: ${theme.colors.text.white};
`;