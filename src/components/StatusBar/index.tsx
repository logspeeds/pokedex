import React from "react";
import * as S from './styles'

type Type = {
    type: string;
    base_stat: number
}

export function StatusBar(type: Type) {

    function progress() {
        let stat = type.base_stat / 2.55
        return stat
    }



    return (
        <S.Container>
            <S.Bar>
                <S.Progress type={type.type} base_stat={progress()}></S.Progress>
            </S.Bar>
        </S.Container>
    )
}