import { Dimensions } from "react-native";
import styled, { css } from "styled-components/native";
import theme from "../../global/styles/theme";

const windowWidth = Dimensions.get("window").width

export const Container = styled.View`
${({ theme }) => css`
flex: 1;
background-color: ${theme.colors.background.white};
`}
`;
export const Content = styled.View`
height: 70%;
justify-content: center;
align-items: center;
`;

export const Header = styled.ImageBackground`
${({ theme }) => css`
width: ${windowWidth}px;
height: 220px;
background-color: ${theme.colors.background.white};
margin-left: -20px;
`}
`;

export const Title = styled.Text`
${({ theme }) => css`
font-size: 32px;
line-height: 38px;
font-weight: bold;
color: ${theme.colors.text.grey};
`}
`;