import styled, { css } from "styled-components/native";
import { TypeName } from "./index(TESTE)";
import theme from "../../global/styles/theme";

type TypeProps = {
    type: TypeName
}

export const Header = styled.View <TypeProps>`
${({ theme, type }) => css`
    background-color: ${theme.colors.background.type[type]};

    height: 340px;
    padding: 20px;

    flex-direction: row;
    align-items: center;

    position: relative;
`}
`

export const Backbutton = styled.TouchableOpacity`
position: absolute;
top: 70px;
left: 40px;
`

export const ContentImage = styled.View`
position: relative;
`

export const CircleImage = styled.Image`
width: 125px;
height: 125px;
position: absolute;
`

export const PokemonImage = styled.Image`
width: 125px;
height: 125px;
`

export const Content = styled.View`
margin-left: 30px;
`

export const PokemonId = styled.Text`
${({ theme }) => css`
font-size:  16px;
line-height:  19px;
font-style: normal;
font-weight: bold;
color: ${theme.colors.text.grey};
`}
`

export const PokemonName = styled.Text`
${({ theme }) => css`
text-transform: capitalize;
font-style: normal;
font-weight: bold;
font-size: 28px;
line-height: 38px;
color: ${theme.colors.text.white};
`}
`

export const PokemonTypeContainer = styled.View`
flex-direction: row;
`

export const PokemonType = styled.View <TypeProps>`
${({ theme, type }) => css`
    background-color: ${theme.colors.type[type]};

    flex-direction: row;

    width: 61px;
    height: 25px;

    border-radius: 3px;
    justify-content: center;
    align-items: center;

    margin-right: 10px;
`}
`

export const PokemonTypeText = styled.Text`
${({ theme }) => css`
color: ${theme.colors.text.white};
font-weight: 500;
font-size: 12px;
line-height: 14px;
font-style: normal;
`}
`

export const DotsImage = styled.Image`
width: 85px;
position: absolute;
right: -20px;
top: 220px;
`

export const Container = styled.View`
${({ theme }) => css`
flex: 1;
padding: 20px;
border-top-right-radius: 40px;
border-top-left-radius: 40px;
margin-top: -40px;
background-color: ${theme.colors.background.white};
`}
`

export const Title = styled.Text <TypeProps>`
${({ theme, type }) => css`
    color: ${theme.colors.background.type[type]};

font-style: normal;
font-weight: bold;
font-size: 16px;
line-height: 19px;
padding: 10px 20px;
`}
`

export const StatusBar = styled.View`
width: 100%;
padding: 10px 20px;
flex-direction: row;
justify-content: space-between;
align-items: center;
`

export const Attributes = styled.Text`
${({ theme }) => css`
color: ${theme.colors.text.black};
font-weight: 500;
font-size: 12px;
line-height: 14px;
font-style: normal;
text-transform: capitalize;
width: 100px;
`}
`

export const AttributeValue = styled.Text`
${({ theme }) => css`
color: ${theme.colors.text.number};
font-weight: normal;
font-size: 16px;
line-height: 19px;
font-style: normal;
text-align: right;
text-transform: capitalize;
margin-left: 20px;
width: 36px;
height: 19px;
`}
`

export const AttributeTotalValue = styled.Text`
${({ theme }) => css`
color: ${theme.colors.text.number};
font-weight: 700;
font-size: 16px;
line-height: 19px;
font-style: normal;
text-align: right;
text-transform: capitalize;
margin-left: 20px;
width: 36px;
height: 19px;
`}
`

export const AttributeMaxMinValue = styled.Text`
${({ theme }) => css`
color: ${theme.colors.text.number};
font-weight: normal;
font-size: 16px;
line-height: 19px;
font-style: normal;
text-transform: capitalize;
margin-right: 20px;
text-align: right;
width: 30px;
height: 19px;
`}
`
export const AttributeMaxMinValueText = styled.Text`
${({ theme }) => css`
color: ${theme.colors.text.number};
font-weight: 500;
font-size: 12px;
line-height: 14px;
font-style: normal;
text-transform: capitalize;
margin-right: 20px;
text-align: right;
width: 30px;
height: 19px;

`}
`

export const ProgressBar = styled.View`
${({ theme }) => css`
border-radius: 2px;
margin-left: 20px;
height: 4px;
width: 70px;
`}
`

export const PokemonImageType = styled.Image`
width: 12px;
height: 12px;
margin-right: 5px;
`
export const Ability = styled.Text`
${({ theme }) => css`
color: ${theme.colors.text.grey};
font-weight: normal;
font-size: 16px;
line-height: 19px;
font-style: normal;
padding:10px 20px;
text-transform: capitalize;
`}
`
export const Information = styled.Text`
${({ theme }) => css`
color: ${theme.colors.text.grey};
font-weight: normal;
font-size: 12px;
line-height: 14px;
font-style: normal;
`}
`
export const BadgeContainer = styled.View`
width: 100%;
margin: 0px 20px;
flex-direction: row;

`

export const Badge = styled.Image`
width: 25px;
height: 25px;
margin: 5px;
gap: 5px;
`
export const BadgeValueContainer = styled.Text`
${({ theme }) => css`
color: ${theme.colors.text.grey};
text-align: center;
font-size: 16px;
line-height: 19px;
width: 25px;
height: 25px;
margin: 5px;
gap: 5px;
`}
`
export const TypeDefenseInformation = styled.Text`
${({ theme }) => css`
color: ${theme.colors.text.grey};
size: 16px;
font-weight: 400;
line-height: 19px;
`}
`