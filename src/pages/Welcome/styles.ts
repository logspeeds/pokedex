import styled, { css } from "styled-components/native";
import theme from "../../global/styles/theme";

export const Container = styled.View`
${({ theme }) => css`
flex: 1;
background-color: ${theme.colors.background.type.water};
`}
`;

export const Content = styled.View`
height: 70%;

justify-content: center;
align-items: center;
`;

export const WrapperAnimation = styled.View`
${({ theme }) => css`
width: 200px;
height: 300px;
border-radius: 100px;

justify-content: center;
align-items: center;

background-color: ${theme.colors.type.water};

transform: rotate(30deg);
`}
`;

export const WrapperImage = styled.View`
transform: rotate(-30deg);
`;

export const Footer = styled.View`
${({ theme }) => css`
height: 30%;
padding: 20px;

align-items: center;
justify-content: center;

border-top-right-radius: 20px;
border-top-left-radius: 20px;
background-color: ${theme.colors.background.white};
`}
`;

export const Title = styled.Text`
margin-top: 20px;
font-size: 40px;
color: ${theme.colors.text.white};
`;

export const Subtitle = styled.Text`
font-size: 16px;
margin-top: 20px;
color: ${theme.colors.text.white};
`;