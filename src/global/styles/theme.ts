export default {
    colors: {
        text: {
            white: '#FFFFFF',
            black: '#17171B',
            grey: '#747476',
            number: '#17171B .6',
        },
        type: {
            bug: '#8CB230',
            dark: '#58575F',
            dragon: '#0F6AC0',
            eletric: '#EED535',
            fairy: '#ED6EC7',
            fighting: '#D04164',
            fire: '#FD7D24',
            flying: '#748FC9',
            ghost: '#556AAE',
            grass: '#62B957',
            ground: '#DD7748',
            ice: '#61CEC0',
            normal: '#9DA0AA',
            poison: '#A552CC',
            psychic: '#EA5D60',
            rock: '#BAAB82',
            steel: '#417D9A',
            water: '#4A90DA',
        },

        background: {
            white: '#FFFFFF',
            defaultInput: '#F2F2F2',
            pressedInput: '#E2E2E2',
            modal: '#17171B.5',

            type: {
                bug: '#8BD674',
                dark: '#6F6E78',
                dragon: '#7383B9',
                eletric: '#F2CB55',
                fairy: '#EBA8C3',
                fighting: '#EB4971',
                fire: '#FFA756',
                flying: '#83A2E3',
                ghost: '#8571BE',
                grass: '#8BBE8A',
                ground: '#F78551',
                ice: '#91D8DF',
                normal: '#B5B9C4',
                poison: '#9F6E97',
                psychic: '#FF6568',
                rock: '#D4C294',
                steel: '#4C91B2',
                water: '#58ABF6',
            }
        }
    }

}

/*
type- dark: '#58575F';
type - dragon: '#0F6AC0';
type - eletric: '#EED535';
type - fairy: '#ED6EC7';
type - fighting: '#D04164';
type - fire: '#FD7D24';
type - flying: '#748FC9';
type - ghost: '#556AAE';
type - grass: '#62B957';
type - ground: '#DD7748';
type - ice: '#61CEC0';
type - normal: '#9DA0AA';
type - poison: '#A552CC';
type - psychic: '#EA5D60';
type - rock: '#BAAB82';
type - steel: '#417D9A';
type - water: '#4A90DA';
*/