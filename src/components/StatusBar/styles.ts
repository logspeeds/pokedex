import styled, { css } from "styled-components/native";
import { TypeName } from "../../pages/About";

type TypeProps = {
    type: TypeName
    base_stat: number
}

export const Container = styled.View`
padding-left: 20px;
`;

export const Bar = styled.View`
border-radius: 2px;
height: 4px;
width: 70px;
`;

export const Progress = styled.View<TypeProps>`
${({ theme, type, base_stat }) => css`

background-color: ${theme.colors.background.type[type]};

border-radius: 2px;
height: 4px;
width: ${base_stat}%;
`}
`;