import React from "react";
import * as S from './styles'

import dotImage from '../../../assets/img/dots.png'
import Pokeball from '../../../assets/img/pokeballCard.png'

import bug from '../../../assets/img/icons/bug.png'
import dark from '../../../assets/img/icons/dark.png'
import dragon from '../../../assets/img/icons/dragon.png'
import eletric from '../../../assets/img/icons/eletric.png'
import fairy from '../../../assets/img/icons/fairy.png'
import fighting from '../../../assets/img/icons/fighting.png'
import fire from '../../../assets/img/icons/fire.png'
import flying from '../../../assets/img/icons/flying.png'
import ghost from '../../../assets/img/icons/ghost.png'
import grass from '../../../assets/img/icons/grass.png'
import ground from '../../../assets/img/icons/ground.png'
import ice from '../../../assets/img/icons/ice.png'
import normal from '../../../assets/img/icons/normal.png'
import poison from '../../../assets/img/icons/poison.png'
import psychic from '../../../assets/img/icons/psychic.png'
import rock from '../../../assets/img/icons/rock.png'
import steel from '../../../assets/img/icons/steel.png'
import water from '../../../assets/img/icons/water.png'

import { TouchableOpacityProps } from "react-native";
import { FadeImageAnimation } from "../FadeImageAnimation"


export type PokemonType = {
    type: {
        name: string;
    }
}

export type Pokemon = {
    name: string;
    url: string;
    id: number;
    types: PokemonType[];
}

type Props = {
    data: Pokemon;
} & TouchableOpacityProps

export function Card({ data, ...rest }: Props) {

    function pokemonImageType(pkmType: string) {
        switch (pkmType) {
            case "bug":
                return (bug)

            case "dark":
                return (dark)

            case "dragon":
                return (dragon)

            case "eletric":
                return (eletric)

            case "fairy":
                return (fairy)

            case "fighting":
                return (fighting)

            case "fire":
                return (fire)

            case "flying":
                return (flying)

            case "ghost":
                return (ghost)

            case "grass":
                return (grass)

            case "ground":
                return (ground)
            case "ice":
                return (ice)

            case "normal":
                return (normal)

            case "poison":
                return (poison)

            case "psychic":
                return (psychic)

            case "rock":
                return (rock)

            case "steel":
                return (steel)

            case "water":
                return (water)

            default:
                console.log("ERRO")
        }
    }

    function PokemonId(dataId: number) {
        let dataIdText
        if (dataId <= 9) {
            dataIdText = "00" + dataId
        } else if (dataId <= 99) {
            dataIdText = "0" + dataId
        }
        else {
            dataIdText = dataId

        } return dataIdText
    }

    return (
        <S.PokemonCard type={data.types[0].type.name} {...rest}>
            <S.LeftSide>
                <S.PokemonId>#{PokemonId(data.id)}</S.PokemonId>
                <S.PokemonName>{data.name}</S.PokemonName>
                <S.ImageCardDetailLeftSide source={dotImage} />
                <S.PokemonContentType>
                    {data.types.map(pokemonType => <S.PokemonType type={pokemonType.type.name} key={pokemonType.type.name}>
                        <S.PokemonImageType source={pokemonImageType(pokemonType.type.name)}></S.PokemonImageType>
                        <S.PokemonTypeText>{pokemonType.type.name}</S.PokemonTypeText>
                    </S.PokemonType>)}
                </S.PokemonContentType>
            </S.LeftSide>
            <S.RightSide>
                <S.PokeballDetail source={Pokeball} />
                <FadeImageAnimation>
                    <S.PokemonImage source={{
                        uri: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${data.id}.png`,
                    }} />
                </FadeImageAnimation>
            </S.RightSide>
        </S.PokemonCard>
    )
}
