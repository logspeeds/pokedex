import React, { useEffect, useState } from "react"
import * as S from './styles'
import api from "../../service/api"
import { FlatList } from "react-native"
import { Card, Pokemon, PokemonType } from "../../components/Cards"
import { FadeAnimation } from "../../components/FadeAnimation"
import PokeballHeader from "../../../assets/img/pokeball.png"
import { useNavigation } from "@react-navigation/native";

type Request = {
    id: number
    types: PokemonType[]
}

export function Home() {
    const { navigate } = useNavigation()
    const [pokemonList, setPokemonList] = useState<Pokemon[]>([])

    useEffect(() => {

        async function getAllPokemons() {
            try {
                const response = await api.get('/pokemon')
                const { results } = response.data

                const payloadPokemons = await Promise.all(
                    results.map(async (pokemon: Pokemon) => {
                        const { id, types } = await getMoreInfo(pokemon.url)
                        return {
                            name: pokemon.name,
                            id,
                            types
                        }
                    })
                )
                setPokemonList(payloadPokemons)
            } catch (error) {
                console.log(error)
            } finally {
                console.log('HOME')
            }
        };

        getAllPokemons()
    }, [])

    async function getMoreInfo(url: string): Promise<Request> {
        const response = await api.get(url)
        const { id, types } = response.data
        return {
            id, types
        }
    }

    function handleNavigation(pkmId: number) {
        navigate('About', {
            pkmId,
        })
    }

    return (
        <S.Container>
            <FlatList
                ListHeaderComponent={
                    <>
                        <S.Header source={PokeballHeader} />
                        <S.Title>POKÉDEX</S.Title>
                    </>
                }
                contentContainerStyle={{
                    paddingHorizontal: 20
                }}
                data={pokemonList}
                keyExtractor={pokemonList => pokemonList.id.toString()}
                renderItem={({ item: pokemonList }) => (
                    <FadeAnimation>
                        <Card data={pokemonList} onPress={() => {
                            handleNavigation(pokemonList.id)
                        }} />
                    </FadeAnimation>
                )}
            />
        </S.Container>
    )
}