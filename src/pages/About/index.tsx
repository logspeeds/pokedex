import React, { useEffect, useState } from "react";
import { useRoute, useNavigation } from "@react-navigation/native";
import * as S from './styles'
import api from "../../service/api";
import { useTheme } from "styled-components";
import { ScrollView, Alert } from "react-native";
import { Feather } from '@expo/vector-icons';

import bug from '../../../assets/img/icons/bug.png'
import dark from '../../../assets/img/icons/dark.png'
import dragon from '../../../assets/img/icons/dragon.png'
import eletric from '../../../assets/img/icons/eletric.png'
import fairy from '../../../assets/img/icons/fairy.png'
import fighting from '../../../assets/img/icons/fighting.png'
import fire from '../../../assets/img/icons/fire.png'
import flying from '../../../assets/img/icons/flying.png'
import ghost from '../../../assets/img/icons/ghost.png'
import grass from '../../../assets/img/icons/grass.png'
import ground from '../../../assets/img/icons/ground.png'
import ice from '../../../assets/img/icons/ice.png'
import normal from '../../../assets/img/icons/normal.png'
import poison from '../../../assets/img/icons/poison.png'
import psychic from '../../../assets/img/icons/psychic.png'
import rock from '../../../assets/img/icons/rock.png'
import steel from '../../../assets/img/icons/steel.png'
import water from '../../../assets/img/icons/water.png'

import bugBadge from '../../../assets/img/badge/bug.png'
import darkBadge from '../../../assets/img/badge/dark.png'
import dragonBadge from '../../../assets/img/badge/dragon.png'
import eletricBadge from '../../../assets/img/badge/eletric.png'
import fairyBadge from '../../../assets/img/badge/fairy.png'
import fightingBadge from '../../../assets/img/badge/fighting.png'
import fireBadge from '../../../assets/img/badge/fire.png'
import flyingBadge from '../../../assets/img/badge/flying.png'
import ghostBadge from '../../../assets/img/badge/ghost.png'
import grassBadge from '../../../assets/img/badge/grass.png'
import groundBadge from '../../../assets/img/badge/ground.png'
import iceBadge from '../../../assets/img/badge/ice.png'
import normalBadge from '../../../assets/img/badge/normal.png'
import poisonBadge from '../../../assets/img/badge/poison.png'
import psychicBadge from '../../../assets/img/badge/psychic.png'
import rockBadge from '../../../assets/img/badge/rock.png'
import steelBadge from '../../../assets/img/badge/steel.png'
import waterBadge from '../../../assets/img/badge/water.png'

import Circle from '../../../assets/img/circle.png'
import { FadeAnimation } from "../../components/FadeAnimation";

import dotImage from '../../../assets/img/dots.png'
import { StatusBar } from "../../components/StatusBar";

type RouteParams = {
    pkmId: number
}

type Stats = {
    base_stat: number;
    stat: {
        name: string;
    }
}

export type TypeName =
    | 'bug'
    | "dark"
    | 'dragon'
    | 'eletric'
    | 'fairy'
    | 'fighting'
    | 'fire'
    | 'flying'
    | 'ghost'
    | 'grass'
    | 'ground'
    | 'ice'
    | 'normal'
    | 'poison'
    | 'psychic'
    | 'rock'
    | 'steel'
    | 'water'

type PokemonType = {
    type: {
        name: TypeName;
    };
    base: number;
}

type PokemonProps = {
    id: number;
    name: string;
    types: PokemonType[];
    stats: Stats[];
    color: string;
}

type typeDefenses = {
    type_name: string;
    value: number;
}

type typeDefensesProps = {
    types: typeDefenses[];
}

export function About() {
    const route = useRoute()
    const { goBack } = useNavigation()
    const { pkmId } = route.params as RouteParams
    const { colors } = useTheme()

    const [load, setLoad] = useState(true)
    const [pokemon, setPokemon] = useState({} as PokemonProps)

    const [typeDefensesComplete, setTypeDefensesComplete]: any = useState(
        {
            types: [
                { type_name: 'normal', value: 1 },
                { type_name: 'fire', value: 1 },
                { type_name: 'water', value: 1 },
                { type_name: 'eletric', value: 1 },
                { type_name: 'grass', value: 1 },
                { type_name: 'ice', value: 1 },
                { type_name: 'fighting', value: 1 },
                { type_name: 'poison', value: 1 },
                { type_name: 'ground', value: 1 },
                { type_name: 'flying', value: 1 },
                { type_name: 'psychic', value: 1 },
                { type_name: 'bug', value: 1 },
                { type_name: 'rock', value: 1 },
                { type_name: 'ghost', value: 1 },
                { type_name: 'dragon', value: 1 },
                { type_name: 'dark', value: 1 },
                { type_name: 'steel', value: 1 },
                { type_name: 'fairy', value: 1 }
            ]
        })

    let typeDefenses_1: typeDefensesProps = {
        types: typeDefensesComplete.types.filter((type: typeDefenses) =>
            (type.type_name == "normal" || type.type_name == 'fire' || type.type_name == 'water' || type.type_name == 'eletric' || type.type_name == 'grass' || type.type_name == 'ice' || type.type_name == 'fighting' || type.type_name == 'poison' || type.type_name == "ground"))
    }

    let typeDefenses_2: typeDefensesProps = {
        types: typeDefensesComplete.types.filter((type: typeDefenses) =>
            (type.type_name == 'flying' || type.type_name == 'psychic' || type.type_name == 'bug' || type.type_name == 'rock' || type.type_name == 'ghost' || type.type_name == 'dragon' || type.type_name == 'dark' || type.type_name == 'steel' || type.type_name == 'fairy'))
    }

    useEffect(() => {
        getPokemonDetail()
    }, [])

    async function getPokemonDetail() {
        try {
            const response = await api.get(`/pokemon/${pkmId}`)
            const { stats, id, name, types } = response.data;
            const currentType = types[0].type.name as TypeName;
            const color = colors.background.type[currentType];
            setPokemon({ stats, id, name, types, color })
            typeDefUpdate(types)
        } catch (error) {
            Alert.alert("Ops, something went wrong")
            console.log(error)
        }
        finally {
            setLoad(false)
        }
    }

    function typeDefUpdate(pkmTypes: PokemonType[]) {
        pkmTypes.map(types => {
            switch (types.type.name) {
                case "normal":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "fighting") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "ghost") {
                                        properties['value'] = index.value * 0;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "fire":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "water" || index.type_name === "ground" || index.type_name === "rock") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "fire" || index.type_name === "grass" || index.type_name === "ice" || index.type_name === "bug" || index.type_name === "steel" || index.type_name === "fairy") {
                                        properties['value'] = index.value / 2;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "water":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "eletric" || index.type_name === "grass") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "fire" || index.type_name === "water" || index.type_name === "ice" || index.type_name === "steel") {
                                        properties['value'] = index.value / 2;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "eletric":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "ground") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "eletric" || index.type_name === "flying" || index.type_name === "steel") {
                                        properties['value'] = index.value / 2;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "grass":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "fire" || index.type_name === "ice" || index.type_name === "poison" || index.type_name === "flying" || index.type_name === "bug") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "water" || index.type_name === "eletric" || index.type_name === "grass" || index.type_name === "ground") {
                                        properties['value'] = index.value / 2;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "ice":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "fire" || index.type_name === "fighting" || index.type_name === "rock" || index.type_name === "steel") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "ice") {
                                        properties['value'] = index.value / 2;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "fighting":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "flying" || index.type_name === "psychic" || index.type_name === "fairy") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "bug" || index.type_name === "rock" || index.type_name === "dark") {
                                        properties['value'] = index.value / 2;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "poison":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "ground" || index.type_name === "psychic") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "grass" || index.type_name === "fighting" || index.type_name === "poison" || index.type_name === "bug" || index.type_name === "fairy") {
                                        properties['value'] = index.value / 2;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "ground":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "water" || index.type_name === "grass" || index.type_name === "ice") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "poison" || index.type_name === "rock") {
                                        properties['value'] = index.value / 2;
                                    }
                                    else if (index.type_name === "eletric") {
                                        properties['value'] = index.value * 0;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "flying":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "eletric" || index.type_name === "ice" || index.type_name === "rock") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "grass" || index.type_name === "fighting" || index.type_name === "bug") {
                                        properties['value'] = index.value / 2;
                                    }
                                    else if (index.type_name === "ground") {
                                        properties['value'] = index.value * 0;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "psychic":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "bug" || index.type_name === "ice" || index.type_name === "ghost" || index.type_name === "dark") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "fighting" || index.type_name === "psychic") {
                                        properties['value'] = index.value / 2;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "bug":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "fire" || index.type_name === "flying" || index.type_name === "rock") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "grass" || index.type_name === "fighting" || index.type_name === "ground") {
                                        properties['value'] = index.value / 2;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "rock":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "water" || index.type_name === "grass" || index.type_name === "fighting" || index.type_name === "ground" || index.type_name === "steel") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "normal" || index.type_name === "fire" || index.type_name === "poison" || index.type_name === "flying") {
                                        properties['value'] = index.value / 2;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "ghost":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "ghost" || index.type_name === "dark") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "poison" || index.type_name === "bug") {
                                        properties['value'] = index.value / 2;
                                    }
                                    else if (index.type_name === "normal" || index.type_name === "fighting") {
                                        properties['value'] = index.value * 0;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "dragon":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "ice" || index.type_name === "dragon" || index.type_name === "fairy") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "fire" || index.type_name === "water" || index.type_name === "eletric" || index.type_name === "grass") {
                                        properties['value'] = index.value / 2;
                                    }
                                    else if (index.type_name === "ground") {
                                        properties['value'] = index.value * 0;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "dark":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "fighting" || index.type_name === "bug" || index.type_name === "fairy") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "ghost" || index.type_name === "dark") {
                                        properties['value'] = index.value / 2;
                                    }
                                    else if (index.type_name === "psychic") {
                                        properties['value'] = index.value * 0;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "steel":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "fire" || index.type_name === "fighting" || index.type_name === "ground") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "normal" || index.type_name === "grass" || index.type_name === "ice" || index.type_name === "flying" || index.type_name === "psychic" || index.type_name === "bug" || index.type_name === "rock" || index.type_name === "dragpn" || index.type_name === "steel" || index.type_name === "fairy") {
                                        properties['value'] = index.value / 2;
                                    }
                                    else if (index.type_name === "poison") {
                                        properties['value'] = index.value * 0;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                case "fairy":
                    setTypeDefensesComplete((prevTypeDefensesComplete: typeDefensesProps) => {
                        return {
                            ...prevTypeDefensesComplete, types:

                                prevTypeDefensesComplete.types.map((index: typeDefenses) => {
                                    let properties = {
                                        'type_name': index.type_name,
                                        'value': index.value
                                    };
                                    if (index.type_name === "poison" || index.type_name === "steel") {
                                        properties['value'] = index.value * 2;
                                    }
                                    else if (index.type_name === "fighting" || index.type_name === "bug" || index.type_name === "dark") {
                                        properties['value'] = index.value / 2;
                                    }
                                    else if (index.type_name === "dragon") {
                                        properties['value'] = index.value * 0;
                                    } return properties;
                                }
                                )
                        }
                    }
                    )
                    break
                default:
                    Alert.alert("Ops, something went wrong (unknown type)")
            }
        })
    }

    function pokemonImageType(pkmType: string) {
        switch (pkmType) {
            case "bug":
                return (bug)

            case "dark":
                return (dark)

            case "dragon":
                return (dragon)

            case "eletric":
                return (eletric)

            case "fairy":
                return (fairy)

            case "fighting":
                return (fighting)

            case "fire":
                return (fire)

            case "flying":
                return (flying)

            case "ghost":
                return (ghost)

            case "grass":
                return (grass)

            case "ground":
                return (ground)

            case "ice":
                return (ice)

            case "normal":
                return (normal)

            case "poison":
                return (poison)

            case "psychic":
                return (psychic)

            case "rock":
                return (rock)

            case "steel":
                return (steel)

            case "water":
                return (water)

            default:
                Alert.alert("Ops, something went wrong (unknown type)")
        }
    }

    function pokemonImageBadge(pkmType: string) {
        switch (pkmType) {
            case "normal":
                return (normalBadge)

            case "fire":
                return (fireBadge)

            case "water":
                return (waterBadge)

            case "eletric":
                return (eletricBadge)

            case "grass":
                return (grassBadge)

            case "ice":
                return (iceBadge)

            case "fighting":
                return (fightingBadge)

            case "poison":
                return (poisonBadge)

            case "ground":
                return (groundBadge)

            case "flying":
                return (flyingBadge)

            case "psychic":
                return (psychicBadge)

            case "bug":
                return (bugBadge)

            case "rock":
                return (rockBadge)

            case "ghost":
                return (ghostBadge)

            case "dragon":
                return (dragonBadge)

            case "dark":
                return (darkBadge)

            case "steel":
                return (steelBadge)

            case "fairy":
                return (fairyBadge)

            default:
                Alert.alert("Ops, something went wrong (unknown type)")
        }
    }

    function PokemonId(dataId: number) {
        let dataIdText
        if (dataId <= 9) {
            dataIdText = "00" + dataId
        } else if (dataId <= 99) {
            dataIdText = "0" + dataId
        }
        else {
            dataIdText = dataId

        } return dataIdText
    }

    function minStatsDetail(props: Stats) {
        let minStats: number = 0
        if (props.stat.name == "hp") {
            const HpMin = Math.floor((0.01 * (2 * props.base_stat + 0 + 0) * 100) + 100 + 10)
            return (
                minStats = HpMin
            )

        } else {
            const OtherMin = Math.floor(Math.floor((0.01 * (2 * props.base_stat + 0 + 0) * 100) + 5) * 90 / 100)
            return (
                minStats = OtherMin
            )

        }
    }

    function maxStatsDetail(props: Stats) {
        let maxStats: number = 0
        if (props.stat.name == "hp") {
            const HpMax = Math.floor((0.01 * (2 * props.base_stat + 31 + Math.floor(0.25 * 252)) * 100) + 100 + 10)

            return (
                maxStats = HpMax
            )

        } else {
            const OtherMax = Math.floor(Math.floor((0.01 * (2 * props.base_stat + 31 + Math.floor(0.25 * 252)) * 100) + 5) * 110 / 100)
            return (
                maxStats = OtherMax
            )

        }
    }

    function sumStats() {
        const array = pokemon.stats
        let sum = 0;

        for (let i = 0; i < array.length; i++) {
            sum += array[i].base_stat;
        }
        return sum
    }

    function characterMod(value: number | string) {
        if (value == "0.5") {
            return value = "½";
        } else if (value == "0.25") {
            return value = "¼";
        } else if (value == '0') {
            return value = 0;
        } else if (value == '2') {
            return value = 2
        } else if (value == '4') {
            return value = 4
        }
    }

    function handleBackButton() {
        goBack()
    }

    return (
        <>
            {load ?
                <>
                </>
                :
                <ScrollView style={{ backgroundColor: '#fff' }}>
                    <S.Header type={pokemon.types[0].type.name}>
                        <S.Backbutton onPress={handleBackButton}>
                            <Feather name="chevron-left" size={24} color="#fff" />
                        </S.Backbutton>
                        <S.ContentImage>
                            <S.CircleImage source={Circle} />
                            <FadeAnimation>
                                <S.PokemonImage source={{ uri: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemon.id}.png` }} />
                            </FadeAnimation>
                        </S.ContentImage>
                        <S.Content>
                            <S.PokemonId>#{PokemonId(pokemon.id)}</S.PokemonId>
                            <S.PokemonName>{pokemon.name}</S.PokemonName>
                            <S.PokemonTypeContainer>
                                {pokemon.types.map(({ type }) =>
                                    <S.PokemonType type={type.name} key={type.name}>
                                        <S.PokemonImageType source={pokemonImageType(type.name)}></S.PokemonImageType>
                                        <S.PokemonTypeText>{type.name}</S.PokemonTypeText>
                                    </S.PokemonType>
                                )}
                            </S.PokemonTypeContainer>
                        </S.Content>
                        <S.DotsImage source={dotImage} />
                    </S.Header>
                    <S.Container>
                        <S.Title type={pokemon.types[0].type.name}>Base Stats</S.Title>
                        {
                            pokemon.stats.map(attribute =>
                                <S.StatusBar key={attribute.stat.name} >
                                    <S.Attributes> {attribute.stat.name} </S.Attributes>
                                    <S.AttributeValue> {attribute.base_stat} </S.AttributeValue>
                                    <StatusBar type={pokemon.types[0].type.name} base_stat={attribute.base_stat} />
                                    <S.AttributeMaxMinValue>{minStatsDetail(attribute)}</S.AttributeMaxMinValue>
                                    <S.AttributeMaxMinValue>{maxStatsDetail(attribute)}</S.AttributeMaxMinValue>
                                </S.StatusBar>
                            )
                        }
                        <S.StatusBar >
                            <S.Attributes> Total </S.Attributes>
                            <S.AttributeTotalValue> {sumStats()} </S.AttributeTotalValue>
                            <S.ProgressBar></S.ProgressBar>
                            <S.AttributeMaxMinValueText>Min</S.AttributeMaxMinValueText>
                            <S.AttributeMaxMinValueText>Max</S.AttributeMaxMinValueText>
                        </S.StatusBar>
                        <S.StatusBar >
                            <S.Information>
                                The ranges shown on the right are for a level 100 Pokémon. Maximum values are based on a beneficial nature, 252 EVs, 31 IVs; minimum values are based on a hindering nature, 0 EVs, 0 IVs.
                            </S.Information>
                        </S.StatusBar>
                        <S.Title type={pokemon.types[0].type.name}>Type Defenses</S.Title>
                        <S.StatusBar >
                            <S.TypeDefenseInformation>
                                The effectiveness of each type on {pokemon.name}.
                            </S.TypeDefenseInformation>
                        </S.StatusBar>
                        <S.BadgeContainer>
                            {typeDefenses_1.types.map(({ type_name }) =>
                                <S.Badge key={type_name} source={pokemonImageBadge(type_name)}></S.Badge>
                            )
                            }
                        </S.BadgeContainer>
                        <S.BadgeContainer>
                            {
                                typeDefenses_1.types.map(attribute =>
                                    <S.BadgeValueContainer key={attribute.type_name}> {characterMod(attribute.value.toString())} </S.BadgeValueContainer>
                                )
                            }
                        </S.BadgeContainer>
                        <S.BadgeContainer>
                            {typeDefenses_2.types.map(({ type_name }) =>
                                <S.Badge key={type_name} source={pokemonImageBadge(type_name)}></S.Badge>
                            )
                            }
                        </S.BadgeContainer>
                        <S.BadgeContainer>
                            {
                                typeDefenses_2.types.map(attribute =>
                                    <S.BadgeValueContainer key={attribute.type_name}> {characterMod(attribute.value.toString())} </S.BadgeValueContainer>
                                )
                            }
                        </S.BadgeContainer>
                    </S.Container>
                </ScrollView>
            }
        </>
    )
}